class CalendarView {
  constructor(dom, option) {
    this.dom = dom;
    this.option = option;
  }

  render() {
    this.initCanvas();
    this.initData();
    this.repaint();
  }

  initCanvas(){
    this.canvas = document.createElement('canvas');
    const style = window.getComputedStyle(this.dom,'null');
    this.canvas.setAttribute('width',style.width);
    this.canvas.setAttribute('height', style.height);
    this.canvas.style.margin = '0';
    this.canvas.style.padding = '0';
    this.dom.appendChild(this.canvas);
    this.ctx = this.canvas.getContext("2d");
  }

  initData() {
    this.data = this.option.series[0].data;
    this.ctx = this.canvas.getContext("2d");
    this.fontSize = 16;
    this.w = (this.canvas.width - 20) / 14;
    this.h = this.canvas.height / 8;
    this.radius = Math.min(this.w * 2 - 10, this.h - 10);
    this.date = new Date(this.option.calendar.range[0]);
    this.getDays(this.date);
  }


  repaint() {
    this.ctx.beginPath();
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.drawLabel(this.fontSize);
    this.drawLegend();
    this.drawGrid();
    this.drawPies();
  }

  getDays(date) {
    this.days = [];
    const monthdays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if (date.getFullYear() % 4 === 0 && date.getFullYear() % 100 !== 0 || date.getFullYear() % 400 === 0) {
      monthdays[1] = 29;
    }
    date.setDate(1)
    let st = date.getDay();
    if (st === 0) {
      st = 6;
    } else {
      st = st - 1;
    }
    let startDay = st;
    let c = 0;
    for (let i = 0; i < monthdays[date.getMonth()]; i++) {
      this.days.push({x: (st % 7) * this.w * 2 + 10, y: this.fontSize * 5 + this.h * c});
      st++;
      if (st % 7 === 0) {
        c++;
      }
    }
  }

  // 绘制网格和边框
  drawGrid() {
    this.days.forEach((item, i) => {
      this.ctx.beginPath();
      this.ctx.strokeStyle = '#ccc';
      this.ctx.lineWidth = 1;
      this.ctx.rect(item.x, item.y, this.w * 2, this.h);
      this.ctx.stroke();
      // 上边界
      if (i < 7) {
        this.ctx.beginPath();
        this.ctx.strokeStyle = 'black';
        this.ctx.lineWidth = 2;
        this.ctx.moveTo(item.x, item.y);
        this.ctx.lineTo(item.x + this.w * 2, item.y);
        this.ctx.stroke();
      }
      // 下边界
      if (this.days.length - i - 1 < 7) {
        this.ctx.beginPath();
        this.ctx.strokeStyle = 'black';
        this.ctx.lineWidth = 2;
        this.ctx.moveTo(item.x, item.y + this.h);
        this.ctx.lineTo(item.x + this.w * 2, item.y + this.h);
        this.ctx.stroke();
      }
      // 左边界
      if (i === 0 || item.x === 10) {
        this.ctx.beginPath();
        this.ctx.strokeStyle = 'black';
        this.ctx.lineWidth = 2;
        this.ctx.moveTo(item.x, item.y);
        this.ctx.lineTo(item.x, item.y + this.h);
        this.ctx.stroke();
      }
      // 右边界
      if (item.x === 10 + this.w * 12 || i === this.days.length - 1) {
        this.ctx.beginPath();
        this.ctx.strokeStyle = 'black';
        this.ctx.lineWidth = 2;
        this.ctx.moveTo(item.x + this.w * 2, item.y);
        this.ctx.lineTo(item.x + this.w * 2, item.y + this.h);
        this.ctx.stroke();
      }
    })
  }

  drawLabel(fontSize) {
    this.ctx.strokeStyle = 'black';
    this.ctx.fillStyle = 'black';
    let labels = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期七']
    this.ctx.font = `${fontSize}px Arial`;
    this.ctx.textAlign = 'center';
    for (let i = 0; i < 7; i++) {
      this.ctx.fillText(labels[i], this.w * (i * 2 + 1), this.fontSize * 4);
    }
  }

  drawLegend() {
    this.option.legend.data.forEach((item, i) => {
      this.ctx.beginPath();
      this.ctx.fillStyle = colorList[i % colorList.length];
      this.ctx.rect(this.w * (i + 2) * 2, this.fontSize, this.fontSize * 2, this.fontSize);
      this.ctx.fill();
      this.ctx.beginPath();
      this.ctx.fillStyle = 'black';
      this.ctx.textBaseline = 'top';
      this.ctx.fillText(item, this.w * (i + 2) * 2 + this.w + this.fontSize / 2, this.fontSize);
      this.ctx.fill();
    })
  }

  drawPies() {
    this.data.forEach((item) => {
      this.dom.style.position = 'relative';
      let dom = document.createElement('div');
      dom.style.width = this.w * 2 + 'px';
      dom.style.height = this.h + 'px';
      dom.style.position = 'absolute';
      dom.style.left = this.days[item.series[0].day].x + 'px';
      dom.style.top = this.days[item.series[0].day].y + 'px';
      this.dom.appendChild(dom);
      const pie = new PieView(dom, item);
      pie.render();
    })
  }
}
