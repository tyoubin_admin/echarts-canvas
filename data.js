const pie = document.getElementById('pie-view');
const radar = document.getElementById('radar-view');
const waterBall = document.getElementById('water-ball-view');
const calendar = document.getElementById('calendar-view');
const progress1 = document.getElementById('progress-view1');
const progress2 = document.getElementById('progress-view2');
const score = document.getElementById('score-view');

const charts = [];
let pieView = new PieView(pie, notePieOption);
pieView.render();

let radarView = new RadarView(radar, radarOption);
radarView.render();

let waterBallView = new WaterBallView(waterBall, waterOption);
waterBallView.render();

let scoreView = new ScoreView(score, scoreGauge);
scoreView.render();

let calendarView = new CalendarView(calendar, calendarOption);
calendarView.render();

let progressView1 = new ProgressView(progress1, progressOptions1);
progressView1.render();

let progressView2 = new ProgressView(progress2, progressOptions2);
progressView2.render();

const timeLineDom = document.getElementById('time-line');
let timeLine = ''
timeLineData.forEach((item) => {
  timeLine= timeLine+ `
        <div class="item">
        <div class="point" style="background: ${colorTimeLine[item.type]}"></div>
        <div>
          <p class="content">${item.content}</p>
          <p class="date"><i>${item.time}</i></p>
        </div>
      </div>`;
})
timeLineDom.innerHTML = timeLine;
