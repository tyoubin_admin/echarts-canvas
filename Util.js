function getOffsetTop(obj) {
  var tmp = obj.offsetTop;
  var val = obj.offsetParent;
  while (val != null) {
    tmp += val.offsetTop;
    val = val.offsetParent;
  }
  return tmp;
}

function getOffsetLeft(obj) {
  let tmp = obj.offsetLeft;
  let val = obj.offsetParent;
  while (val != null) {
    tmp += val.offsetLeft;
    val = val.offsetParent;
  }
  return tmp;
}

function getScroll() {
  return {
    left: window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft || 0,
    top: window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0
  };
}

function getMonthData() {
  let data = [];
  for (let i = 0; i < 31; i++) {
    data.push(
        {
          label: false,
          series: [{
            type: 'pie',
            day: i,
            data: [
              {name: '学习', value: Math.random() * 1000},
              {name: '运动', value: Math.random() * 1000},
              {name: '其他', value: Math.random() * 1000}
            ],
            roseType: 'radius'
          }]
        }
    )
  }
  return data;
}
