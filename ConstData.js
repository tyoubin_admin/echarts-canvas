const colorList = ['#5470c6', '#91cc75', '#fac858', '#ee6666', '#73c0de', '#3ba272', '#fc8452', '#9a60b4', '#ea7ccc']
const colorHoverList = ['#5c7bd9', '#9fe080', '#ffdc60', '#ff7070', '#7ed3f4', '#7ed3f4', '#ff915a', '#a969c6', '#ff88e0']
const colorTimeLine = ['#5c7bd9', '#ffdc60', '#9fe080', '#ff7070']
const notePieOption = {
  series: [{
    type: 'rose',
    data: [
      {value: 335, name: 'JavaScript'},
      {value: 310, name: 'Css'},
      {value: 274, name: 'TypeScript'},
      {value: 235, name: 'NodeJs'},
      {value: 400, name: 'Vue'},
    ],
    roseType: 'radius'
  }]
}
const waterOption = {
  // 主标题文本样式设置
  series: [{
    type: 'liquidFill',
    data: [0.4],
    radius: '45%',
    center: ['8%', '50%'],
    label: {
      fontSize: 10
    },
    outline: {
      borderDistance: 0,
      itemStyle: {
        borderWidth: 3,
        // borderColor: '#156ACF',
        shadowBlur: 20,
      }
    }
  },
    {
      type: 'liquidFill',
      data: [0.6],
      radius: '45%',
      center: ['22%', '50%'],
      label: {
        fontSize: 10,
      },
      outline: {
        borderDistance: 0,
        itemStyle: {
          borderWidth: 3,
          // borderColor: '#156ACF',
          shadowBlur: 20,
        }
      }
    },
    {
      type: 'liquidFill',
      data: [0.6],
      radius: '45%',
      center: ['36%', '50%'],
      label: {
        fontSize: 10
      },
      outline: {
        borderDistance: 0,
        itemStyle: {
          borderWidth: 3,
          // borderColor: '#156ACF',
          shadowBlur: 20,
        }
      }
    },
    {
      type: 'liquidFill',
      data: [0.5],
      radius: '45%',
      center: ['50%', '50%'],
      label: {
        fontSize: 10
      },
      outline: {
        borderDistance: 0,
        itemStyle: {
          borderWidth: 3,
          // borderColor: '#156ACF',
          shadowBlur: 20,
        }
      }
    },
    {
      type: 'liquidFill',
      data: [0.9],
      radius: '45%',
      center: ['64%', '50%'],
      label: {
        fontSize: 10
      },
      outline: {
        borderDistance: 0,
        itemStyle: {
          borderWidth: 3,
          // borderColor: '#156ACF',
          shadowBlur: 20,
        }
      }
    },
    {
      type: 'liquidFill',
      data: [0.0],
      radius: '45%',
      center: ['78%', '50%'],
      label: {
        fontSize: 10
      },
      outline: {
        borderDistance: 0,
        itemStyle: {
          borderWidth: 3,
          // borderColor: '#156ACF',
          shadowBlur: 20,
        }
      }
    },
    {
      type: 'liquidFill',
      data: [0.0],
      radius: '45%',
      center: ['92%', '50%'],
      label: {
        fontSize: 10
      },
      outline: {
        borderDistance: 0,
        itemStyle: {
          borderWidth: 3,
          // borderColor: '#156ACF',
          shadowBlur: 20,
        }
      }
    }
  ]
}
const radarOption = {
  aria: {
    show: false
  },
  radar: {
    indicator: [
      {text: '学习', max: 100},
      {text: '阅读', max: 100},
      {text: '睡眠', max: 100},
      {text: '饮食', max: 100},
      {text: '运动', max: 100},
      {text: '其他', max: 100},
    ],
  },
  series: [{
    name: '计划分布',
    type: 'radar',
    itemStyle: {normal: {areaStyle: {type: 'default'}}},
    data: [
      {
        value: [54, 60, 70, 90, 50, 68],
        name: '各项指标'
      },
    ],
  }]
}
const scoreGauge = {
  tooltip: {
    formatter: '{a} <br/>{b} : {c}%'
  },
  series: [{
    name: '学生成绩',
    type: 'gauge',
    startAngle: 180,
    endAngle: 0,
    min: 0,
    max: 1,
    splitNumber: 8,
    axisLine: {
      lineStyle: {
        width: 5,
        color: [
          [0.25, '#FF6E76'],
          [0.5, '#FDDD60'],
          [0.75, '#58D9F9'],
          [1, '#7CFFB2'],
        ]
      }
    },
    pointer: {
      length: '80%',
      itemStyle: {
        color: 'auto'
      }
    },
    axisTick: {
      length: 15,
      lineStyle: {
        color: 'auto'
      }
    },
    splitLine: {
      length: 20,
      lineStyle: {
        color: 'auto',
        width: 5
      }
    },
    axisLabel: {
      color: '#464646',
      fontSize: 20,
      distance: -60,
      formatter: function (value) {
        if (value === 0.875) {
          return '优'
        } else if (value === 0.625) {
          return '中'
        } else if (value === 0.375) {
          return '良'
        } else if (value === 0.125) {
          return '差'
        }
      }
    },
    title: {
      offsetCenter: [0, '-50%'],
      fontSize: 20,
    },
    detail: {
      valueAnimation: true,
      fontSize: 30,
      offsetCenter: [0, '-30%'],
      formatter: function (value) {
        return Math.round(value * 100) + '%';
      },
      color: 'auto',
    },
    data: [{
      value: 0.75,
      // name: '成绩评定'
    }]
  }]
}
const calendarOption = {
  legend: {
    data: ['学习', '运动', '其他'],
  },
  calendar: {
    range: ['2021-08']
  },
  series: [{
    id: 'label',
    type: 'scatter',
    coordinateSystem: 'calendar',
    symbolSize: 1,
    data: getMonthData()
  }]
}
const progressOptions1 = {
  series: [
    {
      score: 50
    }
  ]
}
const progressOptions2 = {
  series: [
    {
      score: 90
    }
  ]
}
const timeLineData = [
  {
    id: 1,
    type: 0,
    content: '发表计划-读书',
    time: '2021/08/02'
  },
  {
    id: 2,
    type: 1,
    content: '广场收到用户(123456)点赞',
    time: '2021/08/02'
  },
  {
    id: 3,
    type: 0,
    content: '发表计划-运动30分钟',
    time: '2021/08/02'
  },
  {
    id: 4,
    type: 2,
    content: '完成计划-学习html',
    time: '2021/08/02'
  },
  {
    id: 5,
    type: 3,
    content: '计划(吃水果)已过期',
    time: '2021/08/02'
  },
  {
    id: 6,
    type: 0,
    content: '添加笔记-数据结构',
    time: '2021/08/02'
  },
  {
    id: 7,
    type: 0,
    content: '添加笔记分类-前端学习',
    time: '2021/08/02'
  },
  {
    id: 8,
    type: 1,
    content: '在广场发表动态',
    time: '2021/08/02'
  },
  {
    id: 9,
    type: 1,
    content: '广场粉丝人数达到100人',
    time: '2021/08/02'
  },
]
