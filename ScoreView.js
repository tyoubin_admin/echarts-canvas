class ScoreView {
  constructor(dom, option) {
    this.option = option;
    this.dom = dom;
  }

  render() {
    this.initCanvas();
    this.initData();
    this.data.data.forEach((item) => {
      item.timer = setInterval(() => {
        if (item.value < item.mxValue) {
          item.value += 0.01;
          this.repaint();
        } else {
          clearInterval(item.timer);
          item.timer = null;
        }
      }, 10)
    });
  }

  repaint() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.lineWidth = this.data.axisLine.lineStyle.width;
    let colors = this.data.axisLine.lineStyle.color;
    let angle = Math.PI;
    // 画外圆
    for (let i = 0; i < colors.length; i++) {
      this.ctx.strokeStyle = colors[i][1];
      this.ctx.beginPath();
      this.ctx.arc(this.cx, this.cy, this.radius, angle, colors[i][0] * Math.PI + Math.PI);
      this.ctx.stroke();
      angle = colors[i][0] * Math.PI + Math.PI;
    }
    angle = Math.PI;
    let idx = 0;
    for (let i = 0; i < colors.length; i++) {
      this.ctx.strokeStyle = colors[i][1];
      while (idx <= colors[i][0] * 40) {
        this.ctx.beginPath();
        if (idx % 5 === 0) {
          this.ctx.lineWidth = this.data.axisLine.lineStyle.width;
          let sx = Math.cos(angle) * this.radius * 0.8 + this.cx;
          let sy = Math.sin(angle) * this.radius * 0.8 + this.cy;
          let ex = Math.cos(angle) * this.radius * 0.93 + this.cx;
          let ey = Math.sin(angle) * this.radius * 0.93 + this.cy;
          this.ctx.moveTo(sx, sy);
          this.ctx.lineTo(ex, ey);
          this.ctx.stroke();
        } else {
          this.ctx.lineWidth = this.data.axisLine.lineStyle.width / 5;
          let sx = Math.cos(angle) * this.radius * 0.82 + this.cx;
          let sy = Math.sin(angle) * this.radius * 0.82 + this.cy;
          let ex = Math.cos(angle) * this.radius * 0.93 + this.cx;
          let ey = Math.sin(angle) * this.radius * 0.93 + this.cy;
          this.ctx.moveTo(sx, sy);
          this.ctx.lineTo(ex, ey);
          this.ctx.stroke();
        }
        angle += Math.PI / 40;
        this.ctx.lineWidth = this.data.axisLine.lineStyle.width;
        idx++;
      }
      this.ctx.lineWidth = this.data.axisLine.lineStyle.width;
    }
    this.drawLabel(18);
    this.data.data.forEach((item) => {
      this.drawArrow(item);
    })
  }

  initCanvas(){
    this.canvas = document.createElement('canvas');
    const style = window.getComputedStyle(this.dom,'null');
    this.canvas.setAttribute('width',style.width);
    this.canvas.setAttribute('height', style.height);
    this.canvas.style.margin = '0';
    this.canvas.style.padding = '0';
    this.dom.appendChild(this.canvas);
    this.width = this.canvas.width;
    this.height = this.canvas.height;
    this.ctx = this.canvas.getContext("2d");
  }


  initData() {
    this.data = this.option.series[0];
    this.radius = Math.min(this.canvas.width, this.canvas.height) * 0.75;
    this.cx = this.canvas.width / 2;
    this.cy = this.canvas.height * 0.9;
    let colors = this.data.axisLine.lineStyle.color;
    this.data.data.forEach((item) => {
      for (let i = 0; i < colors.length; i++) {
        if (item.value <= colors[i][0]) {
          item.color = colors[i][1];
          item.mxValue = item.value;
          item.value = 0;
          break;
        }
      }
    })
  }

  drawLabel(fontSize) {
    let colors = this.data.axisLine.lineStyle.color;
    let angle = Math.PI;
    const labels = {
      0: '差',
      1: '良',
      2: '中',
      3: '优',
    }
    for (let i = 0; i < 4; i++) {
      angle = (angle + colors[i][0] * Math.PI + Math.PI) / 2;
      let x = Math.cos(angle) * this.radius * 1.1 + this.cx;
      let y = Math.sin(angle) * this.radius * 1.1 + this.cy;
      this.ctx.font = `${fontSize}px Arial`;
      this.ctx.fillStyle = 'black';
      this.ctx.textAlign = x > this.cx ? 'left' : 'right';
      this.ctx.fillText(labels[i], x, y);
      angle = colors[i][0] * Math.PI + Math.PI;
    }
  }

  drawArrow(item) {
    this.ctx.moveTo(this.cx, this.cy);
    this.ctx.fillStyle = item.color;
    this.ctx.strokeStyle = item.color;
    let angle = item.value * Math.PI + Math.PI;
    let x1 = Math.cos(angle - 0.45) * this.radius * 0.1 + this.cx;
    let y1 = Math.sin(angle - 0.45) * this.radius * 0.1 + this.cy;
    let x2 = Math.cos(angle) * this.radius * 0.8 + this.cx;
    let y2 = Math.sin(angle) * this.radius * 0.8 + this.cy;
    let x3 = Math.cos(angle + 0.45) * this.radius * 0.1 + this.cx;
    let y3 = Math.sin(angle + 0.45) * this.radius * 0.1 + this.cy;
    this.ctx.beginPath();
    this.ctx.moveTo(this.cx, this.cy);
    this.ctx.lineTo(x1, y1);
    this.ctx.lineTo(x2, y2);
    this.ctx.lineTo(x3, y3);
    this.ctx.closePath();
    this.ctx.fill();
    this.ctx.textAlign = 'center';
    this.ctx.font = `bold 50px Arial`;
    this.ctx.fillText(Math.floor(item.value * 100 )+ '%', this.cx, this.cy - 50);
  }
}
