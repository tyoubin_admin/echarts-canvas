class WaterBallView {
  constructor(dom, option) {
    this.option = option;
    this.dom = dom;
  }

  render() {
    this.initCanvas();
    this.initData();
    this.data.forEach((item) => {
      item.timer = setInterval(() => {
        if (item.value < item.mxValue) {
          item.value += 0.01;
          this.repaint();
        } else {
          clearInterval(item.timer);
          item.timer = null;
        }
      }, 10);
    })
    requestAnimationFrame(this.startWave.bind(this));
  }

  repaint() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.data.forEach((item) => {
      this.ctx.beginPath();
      this.ctx.fillStyle = item.color;
      this.ctx.strokeStyle = item.color;
      this.ctx.arc(item.cx, item.cy, item.radius, 0, 2 * Math.PI);
      this.ctx.lineWidth = item.outline.itemStyle.borderWidth;
      this.ctx.stroke();
      this.drawWaterLines(item);
    })
  }

  clear() {
    this.ctx.beginPath();
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }


  initCanvas() {
    this.canvas = document.createElement('canvas');
    const style = window.getComputedStyle(this.dom, 'null');
    this.canvas.setAttribute('width', style.width);
    this.canvas.setAttribute('height', style.height);
    this.canvas.style.margin = '0';
    this.canvas.style.padding = '0';
    this.dom.appendChild(this.canvas);
    this.width = this.canvas.width;
    this.height = this.canvas.height;
    this.ctx = this.canvas.getContext("2d");
  }

  initData() {
    this.data = this.option.series;
    this.maxRadius = Math.min(this.canvas.width, this.canvas.height) * 0.5;
    this.data.forEach((item, i) => {
      item.cx = this.width * parseFloat(item.center[0].split('%')[0]) / 100;
      item.cy = this.height * parseFloat(item.center[1].split('%')[0]) / 100;
      item.radius = item.radius.split('%')[0] / 100 * this.maxRadius;
      item.color = colorList[i % colorList.length];
      item.value = 0;
      item.mxValue = item.data[0];
      item.theta = Math.random();
    })
  }

  drawWaterLines(item) {
    let offset;
    let A = 4;
    let y, x, w;
    w = 4 / item.radius;
    let r = item.radius;
    this.ctx.beginPath();
    this.ctx.fillStyle = 'white';
    this.ctx.arc(item.cx, item.cy, item.radius * 0.9, 0, Math.PI * 2);
    this.ctx.fill();
    this.ctx.save();
    this.ctx.arc(item.cx, item.cy, item.radius * 0.9, 0, Math.PI * 2);
    this.ctx.clip();
    // 每次绘制时水波的偏移距离
    offset = item.cy - item.value * r * 2 + item.radius;
    this.ctx.fillStyle = item.color + 'bb';
    this.ctx.beginPath();
    for (x = 0; x <= 2 * r; x += 0.1) {
      y = A * Math.sin(x * w + item.theta) + offset;
      this.ctx.lineTo(item.cx - item.radius + x, y);
    }
    this.ctx.lineTo(item.cx + item.radius, item.cy + item.radius);
    this.ctx.lineTo(item.cx - item.radius, item.cy + item.radius);
    this.ctx.lineTo(item.cx - item.radius, A * Math.sin(item.theta) + offset);
    this.ctx.closePath();
    this.ctx.fill();
    this.ctx.fillStyle = item.color;
    this.ctx.font = `${item.label.fontSize * 2}px Arial`;
    this.ctx.textAlign = 'center';
    this.ctx.fillText(item.mxValue * 100 + '%', item.cx, item.cy + item.label.fontSize / 2);
    this.ctx.clip();
    this.ctx.font = `${item.label.fontSize * 2}px Arial`;
    this.ctx.textAlign = 'center';
    this.ctx.fillStyle = 'white';
    this.ctx.fillText(item.mxValue * 100 + '%', item.cx, item.cy + item.label.fontSize / 2);
    this.ctx.restore();
  }

  startWave() {
    this.repaint();
    this.data.forEach((item) => {
      item.theta -= 0.1;
      this.drawWaterLines(item);
    })
    requestAnimationFrame(this.startWave.bind(this));
  }
}
